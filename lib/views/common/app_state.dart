import 'package:flutter/material.dart';

abstract class AppState<T extends StatefulWidget> extends State<T> {
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: onWillPop,
        child: Directionality(
            textDirection: TextDirection.rtl,
            child: Scaffold(
                key: scaffoldKey,
                appBar: AppBar(
                  title: Text(this.title()),
                ),
                body: buildView(context))));
  }

  Future<bool> onWillPop() {
    return Future.value(true);
  }

  @required
  String title();

  @required
  Widget buildView(BuildContext context);

  void notifySuccess(String message) {
    final snackBar = SnackBar(
        content: Directionality(
            textDirection: TextDirection.rtl,
            child: Text(
              message,
              // style: TextStyle(fontFamily: "IranSans"),
            )));
    scaffoldKey.currentState.showSnackBar(snackBar);
  }

  void notifyError(String message) {
    final snackBar = SnackBar(
        content: Directionality(
            textDirection: TextDirection.rtl,
            child: Text(
              message,
              // style: TextStyle(fontFamily: "IranSans"),
            )));
    scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
