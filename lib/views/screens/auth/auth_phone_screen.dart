import 'package:flutter/material.dart';

import 'package:sarbaz_maaher/views/common/app_state.dart';
import 'package:sarbaz_maaher/views/screens/auth/subviews/phone_views.dart';

class AuthPhoneScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return  _AuthPhoneScreen();
  }
}

class _AuthPhoneScreen extends AppState<AuthPhoneScreen> {
  bool _isLoading = false;

  @override
  Widget buildView(BuildContext context) {
    return GestureDetector(onTap:(){
      FocusScope.of(context).requestFocus(new FocusNode());
    } ,
        child: Stack(
          children: <Widget>[
            Positioned(
              left: 10,
              right: 10,
              bottom: 20,
              height: 55,
              child: AuthPhoneButton(),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                AuthLoginTextWidget("لطفا شماره موبایل خود را وارد نمایید",24.0),
                AuthPhoneFieldWidget("شماره تلفن", "۰۹۱۲۱۱۲۲۳۳۳",
                    this._isLoading),
              ],
            )
          ],
        )
    );
  }

  @override
  Future<bool> onWillPop() {
    Scaffold.of(context).showSnackBar( SnackBar(
      content:  Text("on back presed"),
    ));

    return Future.value(false);
  }

  @override
  String title() {
    return "ورود به سرباز ماهر ";
  }
}
