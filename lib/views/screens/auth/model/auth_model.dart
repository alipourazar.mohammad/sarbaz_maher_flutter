class AuthPhoneModel {
  final String error;
  final String code;
  final bool result;

  AuthPhoneModel({this.error, this.code, this.result});

  factory AuthPhoneModel.fromJson(Map<String, dynamic> json) {
    return AuthPhoneModel(
      error: json['error'] as String,
      code: json['code'] as String,
      result: json['result'] as bool,
    );
  }
}
