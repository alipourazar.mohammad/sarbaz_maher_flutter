
import 'package:shared_preferences/shared_preferences.dart';
class AuthService {
   Future<bool> isUserLogedIn() async {
    SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getBool("isLogin") ?? false;
  }

  void setUserLogedIn() async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    pref.setBool("isLogin", true);
  }


}
