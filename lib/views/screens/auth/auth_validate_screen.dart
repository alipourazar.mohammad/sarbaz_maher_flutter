import 'package:flutter/material.dart';
import 'package:sarbaz_maaher/views/common/app_state.dart';
import 'package:sarbaz_maaher/views/screens/auth/subviews/phone_views.dart';
import 'package:sarbaz_maaher/views/screens/auth/subviews/validate_views.dart';

class AuthValidateScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthValidateScreen();
  }
}

class _AuthValidateScreen extends AppState<AuthValidateScreen> {
  @override
  Widget buildView(BuildContext context) {
    return Stack(
      children: <Widget>[
        ListView(
          padding: EdgeInsets.only(bottom: 80.0),
          children: <Widget>[
            AuthLoginTextWidget("لطفا کد پیامک شده را وارد کنید", 0.0),
            ValidateEditPhoneNumber('09362890534'),
            AuthValidateFieldWidget("کد", "1111", false),
            Timer(),
          ],
        ),
        Positioned(
          left: 10,
          right: 10,
          bottom: 20,
          height: 55,
          child: AuthValidateButton(),
        )
      ],
    );
  }

  @override
  String title() {
    return 'ورود';
  }
}
