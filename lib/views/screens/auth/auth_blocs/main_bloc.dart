

import 'package:rxdart/rxdart.dart';
import 'package:sarbaz_maaher/views/screens/auth/service/auth_service.dart';


class MainBloc{

  static MainBloc  mainBloc =  MainBloc();

  AuthService authService = AuthService();
  PublishSubject<bool> _isLoginSubject = PublishSubject<bool>();

  Subject<bool> get isLoginSubjectStream => _isLoginSubject;

  MainBloc(){
    authService.isUserLogedIn().then((status) {

      return _isLoginSubject.add(status);
    }
    );
  }

}