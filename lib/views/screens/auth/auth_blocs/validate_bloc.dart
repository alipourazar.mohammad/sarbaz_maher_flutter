import 'package:rxdart/rxdart.dart';
import 'package:sarbaz_maaher/infrastructure/validators.dart';

class ValidateBloc extends Object with Validators {
  final _code = BehaviorSubject<String>(seedValue: null);

  Observable<String> get codeStream =>_code.map(validateCode);

  Function(String ) get changeCodeStream =>_code.add;


  Stream<String> get isButtonEnabled{
    return _code.map(validateCode);
  }

}
final validateBloc = ValidateBloc();