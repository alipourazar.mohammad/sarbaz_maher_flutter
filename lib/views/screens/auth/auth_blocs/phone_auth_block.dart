import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';
import 'package:sarbaz_maaher/infrastructure/api_provider.dart';
import 'package:sarbaz_maaher/infrastructure/api_state.dart';
import 'package:sarbaz_maaher/infrastructure/validators.dart';
import 'package:sarbaz_maaher/views/screens/auth/service/auth_service.dart';
import 'package:sarbaz_maaher/views/screens/auth/auth_validate_screen.dart';
import 'package:sarbaz_maaher/views/screens/auth/model/auth_model.dart';

class AuthBlock extends Object with Validators {
  final _phoneNumber = BehaviorSubject<String>();
  final _isLoading = BehaviorSubject<bool>(seedValue: false);

  //add data to stream
  Observable<String> get phoneNumberStream {
    return _phoneNumber.map(validatePhone);
  }

  //change data
  Function(String) get changePhoneNumberStream => _phoneNumber.add;


  Stream<ApiState> get isButtonEnabled {
    return Observable.combineLatest2(
        _phoneNumber.map(validatePhone), _isLoading, (phoneErr, loading) {
      if (loading) {
        return ApiState.loading;
      }
      if (phoneErr != null) {
        return ApiState.error;
      }
      return ApiState.success;
    });
  }

  sendData(BuildContext context) async {
    try {
      this._isLoading.add(true);

      final validPhoneNumber = _phoneNumber.value;
      var reponse = await ApiProvider.sendPhoneNumber(validPhoneNumber);
      this._isLoading.add(false);
      print('${reponse.body}');
      if (reponse.statusCode == 200) {
        var authPhoneMode = AuthPhoneModel.fromJson(json.decode(reponse.body));

        var authSerVice=AuthService();
        authSerVice.setUserLogedIn();

        if (authPhoneMode.result) {
          Scaffold.of(context).showSnackBar(
              SnackBar(content: Text("در خواست با موفقیت ثبت شد ")));

          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => AuthValidateScreen()),
          );

        } else {
          Scaffold.of(context)
              .showSnackBar(SnackBar(content: Text('${authPhoneMode.error}')));
        }
      }
    } catch (e) {
      this._isLoading.add(false);
      Scaffold.of(context).showSnackBar(SnackBar(content: Text("مشکل در برقراری ارتباط با اینترنت")));
      throw new Exception(e.toString());
    }
  }

  dispose() {
    _phoneNumber.close();
  }
}

final authBloc = AuthBlock();
