import 'package:flutter/material.dart';
import 'package:sarbaz_maaher/views/screens/auth/auth_blocs/validate_bloc.dart';
import 'package:sarbaz_maaher/views/screens/auth/auth_profile_screen.dart';
import 'package:simple_countdown/simple_countdown.dart';

class ValidateEditPhoneNumber extends StatelessWidget {
  final String phoneNumber;

  ValidateEditPhoneNumber(this.phoneNumber);

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 12.0, 0.0),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            IconButton(icon: const Icon(Icons.mode_edit), onPressed: null),
            Text(
              '$phoneNumber',
            ),
          ],
        ));
  }
}

class AuthValidateFieldWidget extends StatelessWidget {
  final bool isLoading;
  final String hint;
  final String label;

  AuthValidateFieldWidget(this.label, this.hint, this.isLoading);

  @override
  Widget build(BuildContext context) {
   return StreamBuilder(stream: validateBloc.isButtonEnabled,builder: (context,snapshot){
       return Container(
         padding: EdgeInsets.fromLTRB(40.0, 36.0, 40.0, 0.0),
         child: TextField(
           onChanged: validateBloc.changeCodeStream,
           maxLength: 4,
           enabled: !isLoading,
           keyboardType: TextInputType.number,
           decoration: InputDecoration(
             border: OutlineInputBorder(),
             hintText: this.hint,
             labelText: this.label,
             errorText: snapshot.data

           ),
         ));
   });
  }
}

class Timer extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Timer();
  }
}

class _Timer extends State<Timer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(40.0, 0.0, 50.0, 0.0),
      child: Row(
        children: <Widget>[
          Text('کد برای شما ارسال میشود  '),
          Countdown(onFinish: (){
            Scaffold.of(context).showSnackBar(new SnackBar(
              content: new Text("time finished"),
            ));
          },
            seconds: 5,
          )
        ],
      ),
    );
  }
}

class AuthValidateButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: validateBloc.codeStream,
      builder: (context, snapshot) {
        var isEnabledString =  snapshot.data ;
        bool isEnabled=false;
        if(isEnabledString==null){
          isEnabled=true;
        }else{
          isEnabled=false;
        }
        return RaisedButton(
          onPressed:  isEnabled ? () {
            Scaffold.of(context).showSnackBar(new SnackBar(
              content: new Text("Sending Message"),
            ));

            navigateToAuthProfileScreen(context);
          } : null,

          color: Colors.blue,
          child: Text(
            "بعدی",
            style: TextStyle(color: Colors.white, fontSize: 16),
          ),

        );
      },
    );
  }

  void navigateToAuthProfileScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AuthProfileScreen()),
    );
  }
}
