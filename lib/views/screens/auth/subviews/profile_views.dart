import 'package:flutter/material.dart';
import 'package:sarbaz_maaher/views/screens/main/home/home_screen.dart';
import 'package:sarbaz_maaher/views/screens/main/main_screen.dart';

class AuthProfileTitleText extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(30.0),
      child: Text(
        'برای ورود به سرباز ماهر اطلاعات زیر را تکمیل کنید',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
    );
  }
}

class AuthProfileFieldWidget extends StatelessWidget {
  final String hint;
  final String label;
  final int maxLength;

  AuthProfileFieldWidget(this.label, this.hint, this.maxLength);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(40.0, 20.0, 40.0, 0.0),
      child: TextFormField(
        maxLength: maxLength,
        enabled: true,
        keyboardType: TextInputType.numberWithOptions(),
        decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: this.hint,
            labelText: this.label),
      ),
    );
  }
}

class AuthProfileButton extends StatelessWidget {
  var isEnabled=true;
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed:
      isEnabled ?() {
        Scaffold.of(context).showSnackBar(new SnackBar(
          content: new Text("hhhhhhhhhhhhhhhhhhhhhhhhhhhhh"),
        ));
        navigateToHomeScreenProfileScreen(context);
      }:null,
      color: Colors.blue,
      child: Text(
        "ورود",
        style: TextStyle(color: Colors.white, fontSize: 16),
      ),
    );
  }

  void navigateToHomeScreenProfileScreen(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RootScreen()),
    );


  }

}
