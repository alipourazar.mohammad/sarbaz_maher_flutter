import 'package:flutter/material.dart';
import 'package:sarbaz_maaher/infrastructure/api_state.dart';
import 'package:sarbaz_maaher/views/screens/auth/auth_blocs/phone_auth_block.dart';

class AuthLoginTextWidget extends StatelessWidget {
  final String text;
  final bottomMargin;

  AuthLoginTextWidget(this.text, this.bottomMargin);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        this.text,
        style: TextStyle(color: Colors.black54, fontWeight: FontWeight.bold),
        textAlign: TextAlign.right,
      ),
      padding: EdgeInsets.fromLTRB(24, 24, 24, bottomMargin),
    );
  }
}

class AuthPhoneFieldWidget extends StatelessWidget {
  final bool isLoading;
  final String hint;
  final String label;

  AuthPhoneFieldWidget(this.label, this.hint, this.isLoading);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: authBloc.phoneNumberStream,
      builder: (context, snapshot) {
        return Container(
          padding: EdgeInsets.fromLTRB(40.0, 0.0, 40.0, 0.0),
          child: TextField(
            onChanged: authBloc.changePhoneNumberStream,
            maxLength: 11,
            enabled: !isLoading,
            keyboardType: TextInputType.number,
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: this.hint,
                labelText: this.label,
                errorText: snapshot.data),
          ),
        );
      },
    );
  }
}

class AuthPhoneButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: authBloc.isButtonEnabled,
        builder: (context, snapshot) {
          var _data = snapshot.data ?? ApiState.error;
          print("state is $_data");
          switch (_data) {
            case ApiState.success:
              {
                return RaisedButton(
                    color: Colors.blue,
                    child: Text(
                      "بعدی",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: () {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      authBloc.sendData(context);
                    });
              }
              break;

            case ApiState.error:
              {
                return RaisedButton(
                    color: Colors.blue,
                    child: Text(
                      "بعدی",
                      style: TextStyle(color: Colors.white, fontSize: 16),
                    ),
                    onPressed: null);
              }
              break;

            case ApiState.loading:
              {
                return Center(child: CircularProgressIndicator());
              }
              break;

            case ApiState.errorHttpResponse:
              {
                Scaffold.of(context).showSnackBar(new SnackBar(
                  content: new Text("Sending Message"),
                ));
              }
              break;
          }
        });
  }
}
