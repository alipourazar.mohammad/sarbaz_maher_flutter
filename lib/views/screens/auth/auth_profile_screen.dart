import 'package:flutter/material.dart';
import 'package:sarbaz_maaher/views/common/app_state.dart';
import 'package:sarbaz_maaher/views/screens/auth/subviews/profile_views.dart';

class AuthProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthProfileScreen();
  }
}

class _AuthProfileScreen extends AppState<AuthProfileScreen> {
  @override
  Widget buildView(BuildContext context) {
    return Stack(
      children: <Widget>[



        ListView(
          padding: EdgeInsets.only(bottom: 60.0),
          children: <Widget>[
            AuthProfileTitleText(),
            Form(
              child: Column(
                children: <Widget>[
                  AuthProfileFieldWidget("نام", "امیر اردلان", 30),
                  AuthProfileFieldWidget("نام خانوادگی", "محمدیان", 30),
                  AuthProfileFieldWidget("کد ملی", "۱۱۲۲۳۳۴۴۵۵", 10),
                ],
              ),
            ),
          ],
        ),

        Positioned(
          left: 10,
          right: 10,
          bottom: 20,
          height: 55,
          child: AuthProfileButton(),
        ),

      ],
    );
  }

  @override
  String title() {
    return "ورود";
  }
}
