import 'package:flutter/material.dart';
import 'package:sarbaz_maaher/views/screens/main/home/home_screen.dart';

class RootScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _RootScreenState();
  }
}

class _RootScreenState extends State<RootScreen> {
  int _currentIndex = 1;

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: DefaultTabController(
          length: 4,
          child: Scaffold(
            appBar: AppBar(automaticallyImplyLeading: false,
              title: titleForIndex()[_currentIndex],
            ),
            body: children()[_currentIndex],
            bottomNavigationBar: BottomNavigationBar(
              onTap: onTabTapped,
              iconSize: 30,
              type: BottomNavigationBarType.fixed,
              // new
              currentIndex: _currentIndex,
              // new
              items: [
                BottomNavigationBarItem(
                  icon: Icon(Icons.home),
                  title: Container(
                    height: 0.0,
                  ),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.person),
                  title: Container(
                    height: 0.0,
                  ),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.credit_card),
                  title: Container(
                    height: 0.0,
                  ),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.menu),
                  title: Container(
                    height: 0.0,
                  ),
                ),
              ],
            ),
          ),
        ));
  }

  List<Widget> children() {
    return [
      HomeScreen(),
      HomeScreen(),
      HomeScreen(),
      HomeScreen(),
      HomeScreen()
    ];
  }

  List<Widget> titleForIndex() {
    return [
      Text("سرباز ماهر"),
      Text("سرباز ماهر"),
      Text("سرباز ماهر"),
      Text("سرباز ماهر"),
      Text("سرباز ماهر")
    ];
  }

  void onTabTapped(int index) {
    setState(() {
      _currentIndex = index;
    });
  }
}
