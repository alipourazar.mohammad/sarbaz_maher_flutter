import 'package:flutter/material.dart';
import 'package:sarbaz_maaher/views/screens/main/home/home_subviews.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeScreen();
  }
}

class _HomeScreen extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    print("loaded");
    return ListView(
      children: <Widget>[
        CustomSlider(),
        Container(
          padding: EdgeInsets.all(20),
          child: Text('تشویقی ها'),
        ),

        Container(
            height: 80,
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.all(15),
                  width: 90,
                  height: 90,
                  color: Colors.grey,
                ),
                Container(
                  margin: EdgeInsets.all(15),
                  width: 90,
                  height: 90,
                  color: Colors.grey,
                ),
                Container(
                  margin: EdgeInsets.all(15),
                  width: 90,
                  height: 90,
                  color: Colors.grey,
                ),
                Container(
                  margin: EdgeInsets.all(15),
                  width: 90,
                  height: 90,
                  color: Colors.grey,
                ),
                Container(
                  margin: EdgeInsets.all(15),
                  width: 90,
                  height: 90,
                  color: Colors.grey,
                ),
              ],
            )),
        Container(
          padding: EdgeInsets.all(20),
          child: Text('مهارت دارم'),
        ),
        RequestCertificate(),

//      Image.asset("images/aaa.png")
      ],
    );
  }
}
