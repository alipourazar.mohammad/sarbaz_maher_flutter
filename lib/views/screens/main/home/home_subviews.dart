import 'package:flutter/material.dart';
import 'package:carousel_pro/carousel_pro.dart';

class CustomSlider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 220.0,
      child: Carousel(
        images: [
          NetworkImage(
              'https://www.lawsite.ir/wp-content/uploads/2017/04/%D9%88%DA%A9%DB%8C%D9%84-%D9%86%D8%B8%D8%A7%D9%85-%D9%88%D8%B8%DB%8C%D9%81%D9%87.jpg'),
          NetworkImage(
              'http://cdn.pana.ir/Media/Image/1396/11/28/636544787537742884.jpg'),
          NetworkImage(
              'https://media.mehrnews.com/d/2015/03/12/3/1617787.jpg?ts=1486462047399')
        ],
      ),
    );
  }
}

class RequestCertificate extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200.0,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
        Expanded(
            child: Text('درخواست مهارت نامه و اعتبار سنجی',style: TextStyle(fontSize: 20.0),)),
          Container(padding: EdgeInsets.all(20),child: SizedBox(
              height: MediaQuery.of(context).size.width *0.50,
              width: MediaQuery.of(context).size.width *0.50,
              child: Container(
                  padding:
                  EdgeInsets.only(right: 16, bottom: 8, left: 8, top: 8),
                  child: Image.asset('images/first.png'))),)
          
          
        ],
      ),
    );
  }
}
