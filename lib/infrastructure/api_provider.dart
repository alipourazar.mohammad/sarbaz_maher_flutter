import 'dart:collection';
import 'package:http/http.dart' as http;

class ApiProvider {
  static Future<http.Response> sendPhoneNumber(String phoneNumber) {
    Map<String, dynamic> body = HashMap();
    body['phone'] = phoneNumber;
    var url = 'http://api.zaerapp.ir:54251/api/v2/auth/send-code-v2';
    var response = http.post(url,
        headers: {
          "X-Zamanak-Api-Key":
              "b322c0ab4aac208ce10b1b212f7b3de74bfef42cede298ccf9d236fa59a66867"
        },
        body: body);

    return response;
  }
}
