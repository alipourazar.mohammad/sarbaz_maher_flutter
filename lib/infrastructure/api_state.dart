enum ApiState {
  loading,
  error,
  success,
  successHttpResponse,
  errorHttpResponse
}
