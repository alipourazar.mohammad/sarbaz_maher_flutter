import 'package:flutter/material.dart';
import 'package:sarbaz_maaher/views/screens/auth/auth_blocs/main_bloc.dart';
import 'package:sarbaz_maaher/views/screens/auth/auth_phone_screen.dart';
import 'package:sarbaz_maaher/views/screens/auth/auth_validate_screen.dart';

void main() => runApp(Root());

class Root extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return new _RootState();
  }
}

class _RootState extends State<Root> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: navigateToLoginScreen(),
    );
  }

  Widget navigateToLoginScreen() {
    return StreamBuilder(
      stream: MainBloc.mainBloc.isLoginSubjectStream,
      builder: (context, snapshot) {
        if (!snapshot.hasData) {
          return Container();
        }
        if (snapshot.data) {
          return AuthValidateScreen();
        } else {
          return AuthPhoneScreen();
        }
      },
    );
  }
}
